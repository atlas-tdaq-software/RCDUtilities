#ifndef RCDDATARANGE_H
#define RCDDATARANGE_H

//******************************************************************************
// file: RCDDataRange.h
// desc: nice printing of data range (for debugging purposes only!)
// auth: 11/02/03 R. Spiwoks
//******************************************************************************
//
// $Id$

namespace RCD {

// DataRange class -------------------------------------------------------------

class DataRange {

  public:

    DataRange(unsigned int* , int );
   ~DataRange();

    void Dump() const;

  private:
    unsigned int* m_addr;
    int           m_size;

};      // class DataRange

// inline implementations ------------------------------------------------------

inline DataRange::DataRange(unsigned int* addr, int size)
    : m_addr(addr), m_size(size) {

}

inline DataRange::~DataRange() {

}

}      // namespace RCD

#endif // RCDDATARANGE_H
