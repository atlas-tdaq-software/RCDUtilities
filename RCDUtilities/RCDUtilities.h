#ifndef RCDUTILITIES_H
#define RCDUTILITIES_H

//******************************************************************************
// file: RCDUtilities.h
// desc: library for text-based utilities (for testing purposes only!)
// auth: 16/12/02 R. Spiwoks
// modf: 02-FEB-2009 R. Spiwoks - mods necessary for gcc43 + clean-up
//******************************************************************************
//
// $Id$

#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <string>

#include "RCDUtilities/RCDDataRange.h"

#pragma GCC system_header

namespace RCD {

int rcd_log(const char* , const char* , const char* , ...);

#ifdef DEBUG
#define CDBG(fmt,args...) RCD::rcd_log("DEBUG",__PRETTY_FUNCTION__,fmt,args)
#else
#define CDBG(X...)
#endif

//------------------------------------------------------------------------------

#ifndef QUIET
#define COUT(fmt,args...) RCD::rcd_log("INFO ",__PRETTY_FUNCTION__,fmt,args)
#else
#define COUT(X...)
#endif

//------------------------------------------------------------------------------

#ifndef QUIET
#define CERR(fmt,args...) RCD::rcd_log("ERROR",__PRETTY_FUNCTION__,fmt,args)
#else
#define CERR(X...)
#endif

//------------------------------------------------------------------------------

int substitute_variable(std::string&);

}       // namespace RCD

#endif // RCDUTILITIES_H
