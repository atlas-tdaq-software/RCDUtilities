//******************************************************************************
// file: RCDDataRange.cc
// desc: nice printing of data range (for debugging purposes only!)
// auth: 11/02/03 R. Spiwoks
//******************************************************************************
//
// $Id$

#include "RCDUtilities/RCDUtilities.h"

using namespace RCD;

//------------------------------------------------------------------------------

void DataRange::Dump() const {

    int i;

    COUT("addr = %08x, size = %04x",m_addr,m_size);

    for(i=0; i<m_size; i++) {
        if(i%8 == 0) std::printf("%04x",i);
        std::printf(" %08x",m_addr[i]);
        if((i+1)%8 == 0) std::printf("\n");
    }
    if(i%8 != 0) std::printf("\n");
}
