//******************************************************************************
// file: testRCDUtilities.cc
// desc: test of library for RCD utilities; also example file
// auth: 16/12/02 R. Spiwoks
//******************************************************************************

// $Id$

#include "RCDUtilities/RCDUtilities.h"

using namespace RCD;

//------------------------------------------------------------------------------

int main() {

    COUT("","");
    COUT("where am I?","");
    COUT("5 = %d",5);
    
    CDBG("do you see me?","");
    
    CERR("and me?","");
}
