//******************************************************************************
// file: RCDUtilities.cc
// desc: library for text-based utilities (for testing purposes only!)
// auth: 16/12/02 R. Spiwoks
// modf: 02-FEB-2009 R. Spiwoks - mods necessary for gcc43 + clean-up
//******************************************************************************
//
// $Id$

#include "RCDUtilities/RCDUtilities.h"
#include <ctime>

int RCD::rcd_log(const char* lev, const char* loc, const char* fmt, ...) {

    va_list ap;
    char    form[1024];
    int     rtnv;
    time_t  xt;
    tm      xm;

    // intialize argument pointer
    va_start(ap,fmt);

    // get local time
    time(&xt);
    localtime_r(&xt,&xm);

    // initialize format string
    (std::strcmp(fmt,"") == 0) ?
        std::sprintf(form,"%u-%02u-%02u %02u:%02u:%02u - %5s in \"%s\"\n",1900+xm.tm_year,1+xm.tm_mon,xm.tm_mday,xm.tm_hour,xm.tm_min,xm.tm_sec,lev,loc) :
        std::sprintf(form,"%u-%02u-%02u %02u:%02u:%02u - %5s in \"%s\":\n>> %s\n",1900+xm.tm_year,1+xm.tm_mon,xm.tm_mday,xm.tm_hour,xm.tm_min,xm.tm_sec,lev,loc,fmt);

    // call printing function
    if(strcmp(lev,"ERROR") == 0) {
        rtnv = std::vfprintf(stderr,form,ap);
    }
    else {
        rtnv = std::vprintf(form,ap);
    }

    // terminate argument pointer
    va_end(ap);

    return(rtnv);
}

// -----------------------------------------------------------------------------

int RCD::substitute_variable(std::string& fname) {

    std::string::size_type idx,odx;
    std::string var;
    char* env;

    CDBG("string at entry = %s",fname.c_str());

    while((idx = fname.find("${")) != std::string::npos) {
        if((odx = fname.find("}",idx+1)) == std::string::npos) {
            CERR("string \"%s\" contains unmatched \"${\"",fname.c_str());
            return(-1);
        } else {
            var = fname.substr(idx+2,odx-idx-2);
            env = getenv(var.c_str());
            if(!env) {
                CERR("string \"%s\" contains unknown variable \"%s\"",fname.c_str(),var.c_str());
                return(-2);
            } else {
                fname.replace(idx,odx-idx+1,env);
            }
        }
    }

    CDBG("string at exit = %s",fname.c_str());

    return(0);
}
